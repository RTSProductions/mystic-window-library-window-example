build: src/mystic-window-library/mystic-window-library.cpp
	mkdir ./build
	mkdir ./build/mystic-window-library
	cp ./src/library-headers/mystic-window-library.h ./build/mystic-window-library/mystic-window-library.h
	cp ./src/mystic-window-library/mystic-types.h ./build/mystic-window-library/mystic-types.h
	clang++ -shared -fPIC -o ./build/mystic-window-library/libmysticwindowlibrary.so ./src/mystic-window-library/mystic-window-library.cpp -lxcb -lxcb-icccm -lxkbcommon-x11
clean: build/
	rm -r ./build
install: build/mystic-window-library/mystic-window-library.h build/mystic-window-library/libmysticwindowlibrary.so /usr/lib /usr/include
	mkdir /usr/include/mystic-window-library/
	cp -r build/mystic-window-library/mystic-window-library.h /usr/include/mystic-window-library/mystic-window-library.h
	cp -r build/mystic-window-library/mystic-types.h /usr/include/mystic-window-library/mystic-types.h
	cp -r build/mystic-window-library/libmysticwindowlibrary.so /usr/lib/libmysticwindowlibrary.so
uninstall: 
	rm -rf /usr/include/mystic-window-library/
	rm -f /usr/lib/libmysticwindowlibrary.so
