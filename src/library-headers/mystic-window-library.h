/*
 * Copyright (c) 2021, theandor All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef mystic_window_library_header
#define mystic_window_library_header
    #include "mystic-types.h"
    #include <functional>
    mystic_application_t mystic_init_application(std::string title, int width, int height, bool enable_window_resize, framerate_t max_framerate, uint32_t background_color);
    mystic_main_t mystic_main_loop(mystic_application_t application, const std::function<int(mystic_event_t)> callback);
    uint32_t mystic_rgb_values_to_uint32(int red, int green, int blue);
    void mystic_draw_rectangle(mystic_application_t application, int x, int y, int width, int height, uint32_t rgb_color);
    void mystic_draw_oval(mystic_application_t application, int x, int y, int width, int height, uint32_t rgb_color);
    void mystic_draw_text(mystic_application_t application, int x, int y, char *text, uint32_t rgb_color);
#endif
