/*
 * Copyright (c) 2021, theandor All rights reserved. 
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <mystic-window-library/mystic-window-library.h>
#include <stdio.h>
#include <string.h>

uint32_t color;

mystic_application_t application;

int main_loop(mystic_event_t event)
{
    if (event.event_type == MYSTIC_EVENT_WINDOW_EXPOSE)
    {
        mystic_draw_rectangle(application, 200, 200, 100, 100, color);
    }
    return MYSTIC_CONTINUE_LOOP;
}

int main(int argc, char *argv[])
{
    application = mystic_init_application("Mystic Window Library Example", 512, 512, false, MYSTIC_RECOMMENDED_MAX_FRAMERATE, mystic_rgb_values_to_uint32(0, 0, 0));
    color = mystic_rgb_values_to_uint32(95, 0, 255);
    return mystic_main_loop(application, main_loop);
}

