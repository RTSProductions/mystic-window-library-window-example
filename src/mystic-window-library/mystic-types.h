/*
 * Copyright (c) 2021, theandor All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <xcb/xcb.h>
#include <stdint.h>
#include <iostream>

#ifndef mystic_window_library_types_header
#define mystic_window_library_types_header
typedef int (mystic_main_t);
typedef int (mystic_exit_code_t);

mystic_exit_code_t MYSTIC_EXIT_LOOP = 0;
mystic_exit_code_t MYSTIC_CONTINUE_LOOP = -1;

/*
 * mystic events
 */
typedef int (mystic_event_type_t);

typedef struct
{
    mystic_event_type_t event_type;
    int x;
    int y;
    std::string key_name;
} mystic_event_t;

/*
 * Window close event
 */
mystic_event_type_t MYSTIC_EVENT_WINDOW_CLOSE = -0; // window close

/*
 * mouse events
 */
mystic_event_type_t MYSTIC_EVENT_NO_EVENT= 0; // no event

mystic_event_type_t MYSTIC_EVENT_KEYBOARD_KEY_PRESS = 1; // key press
mystic_event_type_t MYSTIC_EVENT_KEYBOARD_KEY_RELEASE = 2; // key release
mystic_event_type_t MYSTIC_EVENT_MOUSE_BUTTON_LEFT_PRESS = 3; // left mouse button press
mystic_event_type_t MYSTIC_EVENT_MOUSE_BUTTON_LEFT_RELEASE = 4; // left mouse button release
mystic_event_type_t MYSTIC_EVENT_MOUSE_BUTTON_MIDDLE_PRESS = 5; // middle mouse button press
mystic_event_type_t MYSTIC_EVENT_MOUSE_BUTTON_MIDDLE_RELEASE = 5; // middle mouse button release
mystic_event_type_t MYSTIC_EVENT_MOUSE_BUTTON_RIGHT_PRESS = 5; // right mouse button press
mystic_event_type_t MYSTIC_EVENT_MOUSE_BUTTON_RIGHT_RELEASE = 6; // right mouse button release
mystic_event_type_t MYSTIC_EVENT_MOUSE_MOVE = 7; // the mouse moved
mystic_event_type_t MYSTIC_EVENT_MOUSE_BUTTON_MIDDLE_MOVE_UP = 8; // the mouse moved
mystic_event_type_t MYSTIC_EVENT_MOUSE_BUTTON_MIDDLE_MOVE_DOWN = 9; // the mouse moved
mystic_event_type_t MYSTIC_EVENT_WINDOW_EXPOSE= 10; // the window was resized or something

/*
 * mystic application types
 */

typedef struct
{
    xcb_connection_t *x_server;

    xcb_screen_iterator_t screen_iterator;

    xcb_screen_t *screen;

    xcb_window_t window;
} xcb_variables_t;

typedef struct
{
    xcb_variables_t xcb_variables;

    std::string root_window_title;

    int root_window_width;

    int root_window_height;

    int max_framerate;
} mystic_application_t;

/*
 * framerate
 */
 
typedef int (framerate_t);
framerate_t MYSTIC_RECOMMENDED_MAX_FRAMERATE = 30;

#endif