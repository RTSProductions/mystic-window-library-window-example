/*
 * Copyright (c) 2021, theandor All rights reserved. 
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdint.h>
#include <string>
#include <iostream>

#include <xcb/xcb.h>
#include <xcb/xcb_atom.h>
#include <xcb/xcb_icccm.h>

#include "mystic-types.h"
#include "mysticmainloop.h"
#include "mystic-graphics.h"

mystic_application_t mystic_init_application(std::string title, int width, int height, bool enable_window_resize, framerate_t max_framerate, uint32_t background_color)
{
    char *win_title = &title[0];

    xcb_connection_t *x_server;
    xcb_screen_iterator_t screen_iterator;
    xcb_screen_t *screen;
    xcb_window_t window;

    x_server = xcb_connect(NULL, NULL);

    if (xcb_connection_has_error(x_server))
    {
		std::cout << "Error: cannot connect to X-Server\n";
		std::exit(1);
    }

    screen_iterator = xcb_setup_roots_iterator (xcb_get_setup (x_server));

    screen = screen_iterator.data;

    window = xcb_generate_id (x_server);

    uint32_t mask = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;

    uint32_t window_properties[2] = {background_color,
        XCB_EVENT_MASK_EXPOSURE       | XCB_EVENT_MASK_BUTTON_PRESS   |
        XCB_EVENT_MASK_BUTTON_RELEASE | XCB_EVENT_MASK_POINTER_MOTION |
        XCB_EVENT_MASK_KEY_PRESS      | XCB_EVENT_MASK_KEY_RELEASE }; // window events and window background color

    xcb_create_window (x_server, XCB_COPY_FROM_PARENT, window, screen->root, 10, 10, width, height, 5, XCB_WINDOW_CLASS_INPUT_OUTPUT, screen->root_visual, mask, window_properties);

    int title_length = title.length();

    if (title_length > 1)
    {
        std::cout << "Debug: Setting the window's title to, \"" << win_title << "\n";
        xcb_change_property(x_server, XCB_PROP_MODE_REPLACE, window, XCB_ATOM_WM_NAME, XCB_ATOM_STRING, 8, title_length, win_title);
    }
    if (title_length < 1)
        std::cout << "Warning: Attempted to set the window's title to a invalid char array with a length less then one.\n";

    xcb_map_window (x_server, window);

    if (!(enable_window_resize))
    {
        xcb_size_hints_t xcb_wm_hints;

        xcb_icccm_size_hints_set_min_size(&xcb_wm_hints, width, height); // set the minimum window size hint

        xcb_icccm_size_hints_set_max_size(&xcb_wm_hints, width, height); // set the maximum window size hint

        xcb_icccm_set_wm_size_hints(x_server, window, XCB_ATOM_WM_NORMAL_HINTS, &xcb_wm_hints); // apply the window size hints
    }

    xcb_flush (x_server); // flush the queue of pending actions

    xcb_variables_t xcb_variables = {x_server, screen_iterator, screen, window};

    mystic_application_t mystic_application = {xcb_variables, title, width, height, max_framerate};

    return mystic_application;
}
