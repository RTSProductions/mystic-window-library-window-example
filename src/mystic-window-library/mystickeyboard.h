/*
 * Copyright (c) 2021, theandor All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>

#include <xkbcommon/xkbcommon-x11.h>

bool xkb_has_setup;
int32_t keyboard;
struct xkb_keymap *keymap;
char keyname[64];

void setup_xkb(xcb_connection_t *x_server)
{
    std::cout << "Debug: Init mystic keyboard\n";
    if (xkb_x11_setup_xkb_extension(x_server, 1, 0, (xkb_x11_setup_xkb_extension_flags) 0, NULL, NULL, NULL, NULL) == 0)
    {
        std::cout << "Error: Failed to setup xkb\n";
        std::exit(1);
    }
    keyboard = xkb_x11_get_core_keyboard_device_id(x_server); // get the keyboard device id
    if (keyboard == -1)
    {
        std::cout << "Error: Failed to find a keyboard\n";
    }
    struct xkb_context *context = xkb_context_new((xkb_context_flags) 0);
    if (context == NULL)
        std::exit(1);
    keymap = xkb_x11_keymap_new_from_device(context, x_server, keyboard, (xkb_keymap_compile_flags) 0); // get the keymap of the keyboard
    if (keymap == NULL)
        std::exit(1);
    xkb_has_setup = true;
}

char *keycode_to_name(mystic_application_t application, int keycode, uint16_t state)
{
    xcb_connection_t *x_server = application.xcb_variables.x_server;

    if (!(xkb_has_setup))
        setup_xkb(x_server);

    struct xkb_state *keystate = xkb_x11_state_new_from_device(keymap, x_server, keyboard); // get the state of the keyboard

    if (keystate != NULL)
        xkb_state_key_get_utf8(keystate, keycode, keyname, 64);

    return keyname; //return XKeysymToString(keysym);
}
