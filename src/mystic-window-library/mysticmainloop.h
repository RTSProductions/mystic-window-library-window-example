/*
 * Copyright (c) 2021, theandor All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <unistd.h>
#include <functional>
#include <iostream>

#include <xcb/xcb.h>
#include <xcb/xcb_atom.h>

#include "mystickeyboard.h"
#include "mystic-types.h"

mystic_event_t _mystic_create_event(mystic_event_type_t event_type, int x, int y, std::string key_name)
{
    mystic_event_t event {event_type, x, y, key_name};
    return event;
}

mystic_main_t mystic_main_loop(mystic_application_t application, const std::function<mystic_main_t(mystic_event_t)> callback)
{
    xcb_generic_event_t *event;

    xcb_connection_t *x_server = application.xcb_variables.x_server;

    xcb_window_t window = application.xcb_variables.window;
    unsigned int frames_per_micro_seconds = ((unsigned int) ( 1000000 / ( application.max_framerate )));

    xcb_intern_atom_cookie_t wm_close_property = xcb_intern_atom(x_server, 0, ((std::string) "WM_PROTOCOLS").length(), "WM_PROTOCOLS");
    xcb_intern_atom_reply_t *wm_close_property_reply = xcb_intern_atom_reply(x_server, wm_close_property, 0);

    xcb_intern_atom_cookie_t wm_close_data = xcb_intern_atom(x_server, 0, ((std::string) "WM_DELETE_WINDOW").length(), "WM_DELETE_WINDOW");
    xcb_intern_atom_reply_t *wm_close_data_reply = xcb_intern_atom_reply(x_server, wm_close_data, 0);

    xcb_change_property(x_server, XCB_PROP_MODE_REPLACE, window, wm_close_property_reply->atom, 4, 32, 1, &wm_close_data_reply->atom);

    xcb_flush(x_server); // flush the queue of pending actions

    bool main_running = true;

    while (main_running)
    {
        mystic_event_t mystic_event;
        event = xcb_poll_for_event (x_server);
        if (event != NULL)
        {
            switch (event->response_type & ~0x80)
            {
                case XCB_CLIENT_MESSAGE: {
                    xcb_client_message_event_t *client_message = (xcb_client_message_event_t *) event;

                    if (client_message->data.data32[0] == wm_close_data_reply->atom)
                    { // window close
                        mystic_event = _mystic_create_event(MYSTIC_EVENT_WINDOW_CLOSE, 0, 0, "none");
                        main_running = false;
                    }
                    break;
                }

                case XCB_EXPOSE: { // window exposed
                    xcb_expose_event_t *expose = (xcb_expose_event_t *) event;

                    mystic_event = _mystic_create_event(MYSTIC_EVENT_WINDOW_EXPOSE, 0, 0, "none");
                    break;
                }
                case XCB_BUTTON_PRESS: {
                    xcb_button_press_event_t *button_press = (xcb_button_press_event_t *) event;

                    switch (button_press->detail)
                    {
                        case 1: { // left click
                            mystic_event = _mystic_create_event(MYSTIC_EVENT_MOUSE_BUTTON_LEFT_PRESS, button_press->event_x, button_press->event_y, "none");
                            break;
                        }
                        case 2: { // middle click
                            mystic_event = _mystic_create_event(MYSTIC_EVENT_MOUSE_BUTTON_MIDDLE_PRESS, button_press->event_x, button_press->event_y, "none");
                            break;
                        }
                        case 3: { // right click
                            mystic_event = _mystic_create_event(MYSTIC_EVENT_MOUSE_BUTTON_RIGHT_PRESS, button_press->event_x, button_press->event_y, "none");
                            break;
                        }
                        case 4: { // wheel button up
                            mystic_event = _mystic_create_event(MYSTIC_EVENT_MOUSE_BUTTON_MIDDLE_MOVE_UP, button_press->event_x, button_press->event_y, "none");
                            break;
                        }
                        case 5: { // wheel button down
                            mystic_event = _mystic_create_event(MYSTIC_EVENT_MOUSE_BUTTON_MIDDLE_MOVE_DOWN, button_press->event_x, button_press->event_y, "none");
                            break;
                        }
                        break;
                    }
                    break;
                }
                case XCB_BUTTON_RELEASE: { // button released
                    xcb_button_release_event_t *button_release = (xcb_button_release_event_t *) event;

                    switch (button_release->detail)
                    {
                        case 1: { // left click
                            mystic_event = _mystic_create_event(MYSTIC_EVENT_MOUSE_BUTTON_LEFT_RELEASE, button_release->event_x, button_release->event_y, "none");
                            break;
                        }
                        case 2: { // middle click
                            mystic_event = _mystic_create_event(MYSTIC_EVENT_MOUSE_BUTTON_MIDDLE_RELEASE, button_release->event_x, button_release->event_y, "none");
                            break;
                        }
                        case 3: { // right click
                            mystic_event = _mystic_create_event(MYSTIC_EVENT_MOUSE_BUTTON_RIGHT_RELEASE, button_release->event_x, button_release->event_y, "none");
                            break;
                        }
                        break;
                    }
                    break;
                }
                case XCB_MOTION_NOTIFY: { // mouse moved
                    xcb_motion_notify_event_t *motion_notify = (xcb_motion_notify_event_t *)event;

                    mystic_event = _mystic_create_event(MYSTIC_EVENT_MOUSE_MOVE, motion_notify->event_x, motion_notify->event_y, "none");
                    break;
                }
                case XCB_KEY_PRESS: { // key press
                    xcb_key_press_event_t *key_press = (xcb_key_press_event_t *) event;

                    mystic_event = _mystic_create_event(MYSTIC_EVENT_KEYBOARD_KEY_PRESS, 0, 0, keycode_to_name(application, key_press->detail, key_press->state));
                    break;
                }
                case XCB_KEY_RELEASE: { // key released
                    xcb_key_release_event_t *key_release = (xcb_key_release_event_t *) event;

                    mystic_event = _mystic_create_event(MYSTIC_EVENT_KEYBOARD_KEY_RELEASE, 0, 0, keycode_to_name(application, key_release->detail, key_release->state));
                    break;
                }

                default: {
                    mystic_event = _mystic_create_event(MYSTIC_EVENT_NO_EVENT, 0, 0, "none");
                    break;
                }
            }
            free (event);
        }
        if (callback(mystic_event) != MYSTIC_CONTINUE_LOOP)
            main_running = false;
        mystic_event = _mystic_create_event(MYSTIC_EVENT_NO_EVENT, 0, 0, "none");
        usleep(frames_per_micro_seconds);
    }
    std::cout << "Debug: Exit\n";
    xcb_disconnect(x_server);
    return 0;
}
