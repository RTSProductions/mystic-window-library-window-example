/*
 * Copyright (c) 2021, theandor All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdbool.h>
#include <iostream>

#include <xcb/xcb.h>
#include <xcb/xproto.h>

#ifndef mystic_window_graphics
#define mystic_window_graphics
bool mystic_graphics_has_run_before;

typedef union {
    struct
    {
        uint8_t blue;

        uint8_t green;

        uint8_t red;
    };
    uint32_t rgb;
} rgb_t;

void _mystic_work_in_progress_message()
{
    std::cout << "Warning: 2d graphic support is currently a work in progress in mystic window library\n";
    mystic_graphics_has_run_before = true;
}

uint32_t mystic_rgb_values_to_uint32(int red, int green, int blue)
{
    if (red > 256)
        red = 256;
    if (blue > 256)
        blue = 256;
    if (green > 256)
        green = 256;
    if (red < 0)
        red = 0;
    if (blue < 0)
        blue = 0;
    if (green < 0)
        green = 0;
    uint8_t _red = (uint8_t) red;
    uint8_t _green = (uint8_t) green;
    uint8_t _blue = (uint8_t) blue;

    rgb_t rgb = {_blue, _green, _red};

    return rgb.rgb;
}

xcb_connection_t *x_server;
xcb_window_t window;

xcb_gcontext_t  foreground;
uint32_t mask;

void mystic_draw_rectangle(mystic_application_t application, int x, int y, int width, int height, uint32_t rgb_color)
{
    int16_t _x = (int16_t) x;
    int16_t _y = (int16_t) y;
    uint16_t _width = (uint16_t) width;
    uint16_t _height = (uint16_t) height;

    if (!(mystic_graphics_has_run_before))
        _mystic_work_in_progress_message();

    xcb_connection_t *x_server = application.xcb_variables.x_server;
    xcb_window_t window = application.xcb_variables.window;

    xcb_gcontext_t foreground = xcb_generate_id (x_server);
    uint32_t mask = XCB_GC_FOREGROUND | XCB_GC_GRAPHICS_EXPOSURES;
    uint32_t graphics_properties[2] = {rgb_color, 0};

    xcb_create_gc (x_server, foreground, window, mask, graphics_properties);

    xcb_rectangle_t shape[] = {
            { _x, _y, _width, _height}
    };

    xcb_poly_fill_rectangle (x_server, window, foreground, 1, shape);

    xcb_flush (x_server);
}

void mystic_draw_oval(mystic_application_t application, int x, int y, int width, int height, uint32_t rgb_color)
{
    int16_t _x = (int16_t) x;
    int16_t _y = (int16_t) y;
    uint16_t _width = (uint16_t) width;
    uint16_t _height = (uint16_t) height;

    if (!(mystic_graphics_has_run_before))
        _mystic_work_in_progress_message();

    xcb_connection_t *x_server = application.xcb_variables.x_server;
    xcb_window_t window = application.xcb_variables.window;

    xcb_gcontext_t foreground = xcb_generate_id (x_server);
    uint32_t mask = XCB_GC_FOREGROUND | XCB_GC_GRAPHICS_EXPOSURES;
    uint32_t graphics_properties[2] = {rgb_color, 0};

    xcb_create_gc (x_server, foreground, window, mask, graphics_properties);

    xcb_arc_t shape[] = {
        {_x, _y, _width, _height, 0, 90 << 8}
    };

    xcb_poly_fill_arc (x_server, window, foreground, 1, shape);

    xcb_flush (x_server);
}

void mystic_draw_text(mystic_application_t application, int x, int y, char *text, uint32_t rgb_color)
    {
    if (!(mystic_graphics_has_run_before))
        _mystic_work_in_progress_message();

    xcb_connection_t *x_server = application.xcb_variables.x_server;
    xcb_window_t window = application.xcb_variables.window;

    xcb_gcontext_t foreground = xcb_generate_id (x_server);
    uint32_t mask = XCB_GC_FOREGROUND | XCB_GC_GRAPHICS_EXPOSURES;
    uint32_t graphics_properties[2] = {rgb_color, 0};

    xcb_create_gc (x_server, foreground, window, mask, graphics_properties);

    int text_length = ((std::string) text).length();

    xcb_image_text_8 (x_server, text_length, window, foreground, x, y, text);

    xcb_flush (x_server);
}
#endif

